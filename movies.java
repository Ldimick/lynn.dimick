package edu.byui.cit360.ldimick;


public class Movies {

    private String title;
    private String director;

    public Movies(String title, String director) {
        this.title = title;
        this.director = director;
    }

    public String toString() {
        return "Title: " + title + " Director: " + director;
    }
}