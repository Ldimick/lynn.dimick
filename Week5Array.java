package edu.byui.cit360.ldimick.week5;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class Week5Array {

@Test
    public void myTestMethod(){

        String[] expectedOutput = {"one", "two", "three"};
        String[] methodOutput = {"one", "two", "three"};
        assertArrayEquals(expectedOutput, methodOutput);

    String[] expectedOutput2 = {"oe", "two", "three"};
    String[] methodOutput2 = {"one", "two", "three"};
    assertArrayEquals(expectedOutput2, methodOutput2);

    }
}
