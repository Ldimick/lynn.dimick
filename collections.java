package edu.byui.cit360.ldimick;

import java.util.*;

public class Collections {

    public static void main(String[] args) {

        System.out.println("-- List og CA Baseball CLubs --");
        List list = new ArrayList();
        list.add("Dodgers");
        list.add("Angels");
        list.add("A's");
        list.add("Padres");
        list.add("Giants");
        list.add("Dodgers");

        for (Object str : list) {
            System.out.println((String) str);
        }

// Remove the AL teams
        System.out.println("Removed the AL teams");
        list.remove(1);
        //when an indexed order is removed the rest of the objects index value is decremented
        list.remove(1);

        for (Object str : list) {
            System.out.println((String) str);
        }

            System.out.println("-- Set --");
            Set set = new TreeSet();
            set.add("Dodgers");
            set.add("Angels");
            set.add("A's");
            set.add("Padres");
            set.add("Giants");
            set.add("Dodgers");

            for (Object str : set) {
                System.out.println((String)str);
            }

//Remove AL Teams
        //use objects instead of index
        //also shows that when you add a duplicate it does not add it. replaces dupe
        System.out.println("Remove the AL Teams");
            set.remove("Angels");
            set.remove("A's");

        for (Object str : set) {
            System.out.println((String)str);
        }

            System.out.println("-- Queue --");
            Queue queue = new PriorityQueue();
            queue.add("Dodgers");
            queue.add("Angels");
            queue.add("A's");
            queue.add("Padres");
            queue.add("Giants");
            queue.add("Dodgers");


        System.out.println("Queue: " + queue);
        //Remove AL Teams
        queue.remove("Angels");
        queue.remove("A's");

        System.out.println("Queue: " + queue);


        Iterator iterator = queue.iterator();
            while (iterator.hasNext()) {
                System.out.println(queue.poll());
            }
        System.out.println("Queue: " + queue);

            System.out.println("-- Map --");
            Map map = new HashMap();
            map.put(1,"Dodgers");
            map.put(2,"Angels");
            map.put(3,"A's");
            map.put(4,"Padres");
            map.put(5,"Giants");
            map.put(6,"Dodgers");

            for (int i = 1; i < 7; i++) {
                String result = (String)map.get(i);
                System.out.println(result);
            }

        System.out.println("Remove key item 3 and list teams");

map.remove(3);
        for (int i = 1; i < 7; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        System.out.println("-- List using Generics --");
        List<Movies> myList = new LinkedList<Movies>();
        myList.add(new Movies("Star Wars: Episode VII - The Force Awakens", "J.J. Abrams"));
        myList.add(new Movies("Jurrasic World", "Colin Trevorrow"));
        myList.add(new Movies("The Dark Knight", "Christopher Nolan"));
        myList.add(new Movies("Shrek 2", "Andrew Adamson"));

        for (Movies dvd : myList) {
            System.out.println(dvd);
        }
        System.out.println("-- List with Item 2 removed --");
        myList.remove(2);
        for (Movies movie : myList) {
            System.out.println(movie);
        }

    }
    }
