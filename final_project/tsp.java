package Final_Project;


import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.lang.System;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.concurrent.Executors.*;


public class tsp {
    /* Distance lookup table
    These are the distances betrween each city: Los Angeles, San Diego, Las Vegas, Salt Lake City, San Francisco
    Phoneix, Denver, Seattle, Reno and Bakersfield
    *
    */
    private static final double[][] distances = { { 0, 120, 271, 688, 381, 372, 1017, 1135, 516, 111 },
            {120,0,332,750,502,366,1078,1255,559,231 },
            { 271,332,0,421,569,302,1078,1255,559,286 },
            { 688,750,421,0,736,663,518,829,518,704},
            { 381,502,570,421,0,753,1247,807,218,283 },
            { 372,366,302,663,753,0,790,1415,739,484 },
            { 1017,1078,748,518,1247,790,0,1304,991,1032 },
            { 1135,1255,1115,829,807,1415,1304,0,704,406 },
            { 516,559,439,518,218,739,991,704,0,484},
            {111,231,286,704,283,484,1029,406,0,1032},};

    // Generic variables
    // Populate a list with the cities
    private static List<City> cities;

    //  variables
    private static List<Route> RouteParams = new ArrayList<Route>();
    private static double shortestCost = Double.MAX_VALUE;
    private static Route shortestRoute;


    /**
     * Main function
     *
     * @param args
     */
    public static void main(String[] args) {
        // Used to calculate execution time
       //Add intro here
       intro();
        long time1 = 0;
        // Used to determine number of times the algorithm should run
        int numIterations = 1;

        for (int i = 0; i < numIterations; i++) {
            long time = System.currentTimeMillis();
            // Run Calcualations



            tryThisRoute();
            System.out.println("\tTime:" + (System.currentTimeMillis() - time) + "ms");
            time1 += System.currentTimeMillis() - time;

        //    time = System.currentTimeMillis();
        }

    }

    /************************************************************************************************************/
    /**
     * Calculate the shortest route using the brute force algorithm
     */



    public static void intro() {
        Scanner myObj = new Scanner(System.in);


        System.out.println("This program will calculate the fastest route between ten cities.\n");
        System.out.println("Since this is a circuitous route it doesn't really matter which city is your\n");
        System.out.println("starting a ending point. It is a closed loop.\n\n");

        int choice = 0;

        do {


            System.out.println(" Selections:");
            System.out.println("1 - List Cities");
            System.out.println("2 - Run the Calculation w/ sleep delay\n");
            System.out.println("3 - Run the Calculation (w/o) Sleep\n");

            try {
                choice = myObj.nextInt();
            } catch (InputMismatchException e) {
                String token = myObj.next();
                throw new InputMismatchException("Attempting to read a 1 or 2");
            } catch (NoSuchElementException e) {
                throw new NoSuchElementException("attempts to read an 'int' value from standard input, "
                        + "but no more tokens are available");
            }
            //  Trap for an error
            if (((choice != 1) && (choice != 2) && (choice != 3))) {
                System.out.println("PLease try again");
                choice = 0;
            }
            if (choice == 1) {
                String[] CityNames = new String[]{"Los Angeles", "San Diego", "Las Vegas", "Salt Lake City", "San Francisco", "Phoenix", "Denver", "Seattle", "Reno", "Bakersfield"};
                for (int i = 0; i < CityNames.length; i++) {
                    System.out.println(CityNames[i]);
                }
                System.out.println();
                choice = 0;
            }
            if (choice == 2) {
                ExecutorService myService  = Executors.newFixedThreadPool(10);
                SleepingLoop loop1 = new SleepingLoop("Object1", 2500, 100);
                myService.execute(loop1);
            }


            if (choice == 3) {
            }
        } while (choice == 0);

    }


    public static void tryThisRoute() {
        System.out.println("This is a Brute Force effort that runs all of the possible combinations.");
        // Setup city list
        resetLists();

        // Remove stoke from permutations as always start and end
        List<Integer> cityNums = new ArrayList<Integer>();
        for (int i = 0; i < 9; i++) {
            cityNums.add(i);
        }

        // Calculate
        permute(new Route(), cityNums, true);
        // Output the number of permutations generated
        System.out.println("\tNUmber of Iterations Tried: " + RouteParams.size());
        findShortestRoute(RouteParams);
    }


    private static void resetLists() {
        RouteParams = new ArrayList<Route>();
       // BaBRouteParams = new ArrayList<Route>();

        cities = new ArrayList<City>();

        // Populate City list
        cities.add(new City("Los Angeles", 0, false));
        cities.add(new City("San Diego", 1, false));
        cities.add(new City("Las Vegas", 2, false));
        cities.add(new City("Salt Lake City", 3, false));
        cities.add(new City("San Francisco", 4, false));
        cities.add(new City("Phoenix", 5, false));
        cities.add(new City("Denver", 6, false));
        cities.add(new City("Seattle", 7, false));
        cities.add(new City("Reno", 8, false));
        cities.add(new City("Bakersfield", 9, true));
    }

    /**
     * Generates all permutations in lexicographic order
     *
     * @param r
     * @param notVisited
     */
    private static void permute(Route r, List<Integer> notVisited, boolean tryThisOne) {
        if (!notVisited.isEmpty()) {

            for (int i = 0; i < notVisited.size(); i++) {
                // Pointer to first city in list
                int temp = notVisited.remove(0);

                Route newRoute = new Route();
                // Lazy copy
                for (City c1 : r.getRoute()) {
                    newRoute.getRoute().add(c1);
                }

                // Add the first city from notVisited to the route
                newRoute.getRoute().add(cities.get(temp));

                if (tryThisOne) {
                    permute(newRoute, notVisited, true);
                }
                notVisited.add(temp);
            }
        } else {
            // Route is complete
            if (tryThisOne) {
                RouteParams.add(r);
            } else {
                // Add stoke to start and end of route
                r.getRoute().add(0, cities.get(9));
                r.getRoute().add(cities.get(9));

            }
        }
    }

    /**
     * Gets the cost of all the routes in the list and outputs the cheapest
     *
     * @param routeList
     */
    private static void findShortestRoute(List<Route> routeList) {
        // Loop through all the permutations
        for (Route r : routeList) {
            // Only used to add stoke to start and end of route
            appendStoke(r);

            if (getRouteCost(r) < shortestCost) {
                shortestCost = getRouteCost(r);
                shortestRoute = r;
            }
        }

        System.out.println("\t" + shortestRoute.toString() + "\n\tMiles: " + shortestCost);
    }

    /**
     * Adds stoke to start and finish of route
     *
     * @param r
     *            route
     */
    private static void appendStoke(Route r) {
        r.getRoute().add(0, cities.get(9));
        r.getRoute().add(cities.get(9));
    }

    /**
     * Gets the cost of traveling between the cities in the route
     *
     * @param r
     * @return tempCost
     */
    private static Double getRouteCost(Route r) {
        double tempCost = 0;
        // Add route costs
        for (int i = 0; i < r.getRoute().size() - 1; i++) {
            tempCost += distances[r.getRoute().get(i).getID()][r.getRoute().get(i + 1).getID()];
        }
        return tempCost;
    }
}
