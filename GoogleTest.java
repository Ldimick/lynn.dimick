package edu.byui.cit360.week4.httpexample;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.byui.cit360.week4.Customer;
import edu.byui.cit360.week4.JSONExample;

import java.net.*;
import java.io.*;
import java.util.*;

public class GoogleTest {
    public static String getHTTPContent(String string) {
        try {

            URL url = new URL(string);
            URLConnection urlCon = url.openConnection();

            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

            InputStream inputStream = urlCon.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);

            int character = reader.read();  // reads a single character
            char[] buffer = new char[4096];
            reader.read(buffer);    // reads to an array of characters
            System.out.println(buffer);

        } catch (IOException e) {
            System.err.println(e.toString());
        }
    return null;
    }
    public static void main(String[] args)
    {
        GoogleTest gt = new GoogleTest();
    gt.getHTTPContent("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=AIzaSyB7x6EuwHY5ibGeFDEOV4xUMq3123G6yDE");
    }

}