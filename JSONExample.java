package edu.byui.cit360.week4;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class JSONExample {

public static String locationToJSON(Customer customer) {

    ObjectMapper mapper = new ObjectMapper();
    String s = "";

    try {
        s = mapper.writeValueAsString(customer);
    }
    catch (JsonProcessingException e) {
        System.err.println(e.toString());
    }
    return s;
    }

    //end of section

    public static Customer JSONToLocation(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }
    //end of section


    public JSONExample() {
    }

    public static void main(String[] args) {
        Customer cust = new Customer();
        cust.setName("Lynn");
        cust.setAddress("6122 Bannock Rd");
        cust.setCity("Westminster");
        cust.setLatitude("43.1123343");
        cust.setLongitude("34.1123343");


        String json = JSONExample.locationToJSON(cust);
        System.out.println(json);

        Customer cust2 = JSONExample.JSONToLocation(json);
        System.out.println(cust2);

        }


}


