import java.util.Scanner;

public class exceptionHandlerDemo {

    public static void main(String[] args) {
        float num1 = 0.0f;
        float num2 = 0.0f;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the numerator");
        num1 = scanner.nextFloat();
        do {
            System.out.println("Please enter a non-zero denominator");
            num2 = scanner.nextFloat();
        }
        while (num2 == 0.0);
if(num2==0.0)
    throw new ArithmeticException();

var result = new doDivision();
System.out.print(result.doDivision(num1, num2));
    }
}