package edu.byui.cit360.ldimick;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class  ExecuteASleepingLoop {

        public static void main(String[] args) {

            ExecutorService myService = Executors.newFixedThreadPool(10);
            SleepingLoop loop1 = new SleepingLoop("Object1", 250, 250);
            SleepingLoop loop2 = new SleepingLoop("Object2", 250, 250);
            SleepingLoop loop3 = new SleepingLoop("Object3", 250, 250);
            SleepingLoop loop4 = new SleepingLoop("Object4", 250, 250);
            SleepingLoop loop5 = new SleepingLoop("Object5", 250, 250);
            SleepingLoop loop6 = new SleepingLoop("Object6", 250, 250);

            myService.execute(loop1);
            myService.execute(loop2);
            myService.execute(loop3);
            myService.execute(loop4);
            myService.execute(loop5);
            myService.execute(loop6);

            myService.shutdown();


        }
}
