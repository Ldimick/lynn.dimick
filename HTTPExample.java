package edu.byui.cit360.week4.httpexample;

import edu.byui.cit360.week4.Customer;
import edu.byui.cit360.week4.JSONExample;

import java.net.*;
import java.io.*;
import java.util.*;



public class HTTPExample {

    public static String getHTTPContent(String string){

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;

            while((line= reader.readLine()) !=null) {
                stringBuilder.append(line + "/n");
                content=stringBuilder.toString();
            }

        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return content;
    }

    public static Map getHttpHeaders(String string) {

        Map hashmap = null;
        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            hashmap = http.getHeaderFields();


        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return hashmap;
    }
    public static void main(String[] args) {

       // System.out.println(HTTPExample.getHTTPContent("https://maps.googleapis.com/maps/api/directions/json?origin=6122%20Bannock%20Rd%20Westminster%20CA%2092683&destination=Montreal&key=AIzaSyB7x6EuwHY5ibGeFDEOV4xUMq3123G6yDE"));

        Map<Integer, List> m = HTTPExample.getHttpHeaders("https://maps.googleapis.com/maps/api/directions/json?origin=6122%20Bannock%20Rd%20Westminster%20CA%2092683&destination=Montreal&key=AIzaSyB7x6EuwHY5ibGeFDEOV4xUMq3123G6yDE");

        for (Map.Entry<Integer, List> entry : m.entrySet()) {
            System.out.println("Key= "+ entry.getKey()+"Value= "+entry.getValue());
        }

    }

    }

