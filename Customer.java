package edu.byui.cit360.week4;

public class Customer {

    private String name;
    private String address;
    private String city;
    private String latitude;
    private String longitude;

    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address=address;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public String getName() {
        return name;
    }
    public String getAddress() {
        return address;
    }
    public String getCity() {
        return city;
    }
    public String getLatitude() {
        return latitude;
    }
    public String getLongitude() {
        return longitude;
    }

    public String toString() {
        return "Name: " + name + " Address: "+address + " City: "+city + " Latitude :" + latitude + " Longitude: "+longitude;
    }

    }


